package unit

import SecondGreatLow
import org.junit.Test
import kotlin.test.assertFailsWith

class SecondGreatLowTest {

    @Test
    fun test_success_1() {
        val input = intArrayOf(1, 42, 42, 180)
        val expectedOutput = intArrayOf(42, 42)
        val output = SecondGreatLow(input).arrayChallenge().output
        assert(output.contentEquals(expectedOutput))
    }

    @Test
    fun test_success_2() {
        val input = intArrayOf(4, 90)
        val expectedOutput = intArrayOf(90, 4)
        val output = SecondGreatLow(input).arrayChallenge().output
        assert(output.contentEquals(expectedOutput))
    }

    @Test
    fun test_fail_1() {
        assertFailsWith<ArrayIndexOutOfBoundsException>(
                message = "No exception found",
                block = {
                    val input = intArrayOf(4)
                    SecondGreatLow(input).arrayChallenge().output
                }
        )
    }
}
