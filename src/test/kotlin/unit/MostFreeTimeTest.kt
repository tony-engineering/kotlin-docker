package unit

import MostFreeTime
import org.junit.Test

class MostFreeTimeTest {

    @Test
    fun test_success_1() {
        val input = arrayListOf("09:10AM-09:50AM", "10:00AM-12:30PM", "02:00PM-02:45PM")
        val expectedOutput = "01:30"
        val output = MostFreeTime(input).compute().output
        assert(expectedOutput == output)
    }

    @Test
    fun test_success_2() {
        val input = arrayListOf("10:00AM-12:30PM", "02:00PM-02:45PM", "09:10AM-09:50AM")
        val expectedOutput = "01:30"
        val output = MostFreeTime(input).compute().output
        assert(expectedOutput == output)
    }

    @Test
    fun test_success_3() {
        val input = arrayListOf("02:00PM-02:45PM", "10:00AM-12:30PM", "09:10AM-09:50AM")
        val expectedOutput = "01:30"
        val output = MostFreeTime(input).compute().output
        assert(expectedOutput == output)
    }

    @Test
    fun test_success_4() {
        val input = arrayListOf("12:15PM-02:00PM", "09:00AM-10:00AM", "10:30AM-12:00PM")
        val expectedOutput = "00:30"
        val output = MostFreeTime(input).compute().output
        assert(expectedOutput == output)
    }

    @Test
    fun test_success_5() {
        val input = arrayListOf("12:15PM-02:00PM", "09:00AM-12:11PM", "02:02PM-04:00PM")
        val expectedOutput = "00:04"
        val output = MostFreeTime(input).compute().output
        assert(expectedOutput == output)
    }

    @Test
    fun test_success_6() {
        val input = arrayListOf("12:15PM-02:00PM", "02:00PM-02:10PM", "02:10PM-04:00PM")
        val expectedOutput = "00:00"
        val output = MostFreeTime(input).compute().output
        assert(expectedOutput == output)
    }

    @Test
    fun test_ordering_1() {
        val input = arrayListOf("02:00PM-02:45PM", "10:00AM-12:30PM", "09:10AM-09:50AM")
        val expectedOutput = arrayListOf("09:10AM-09:50AM", "10:00AM-12:30PM", "02:00PM-02:45PM")
        val output = MostFreeTime(input).orderInput().input
        assert(expectedOutput == output)
    }
}
