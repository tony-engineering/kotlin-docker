FROM maven:3.6.3-jdk-8 AS testable
RUN mkdir /app
WORKDIR /app
COPY pom.xml .
RUN mvn dependency:go-offline
ADD . .
RUN mvn -o test-compile
CMD mvn -o test

FROM testable AS buildable
RUN mvn -o package -DskipTests=true

FROM maven:3.6.3-jdk-8 as deployable
COPY --from=buildable /app/target/test-kotlin-1.0-SNAPSHOT-jar-with-dependencies.jar /app/target/
CMD java -jar /app/target/test-kotlin-1.0-SNAPSHOT-jar-with-dependencies.jar