# test-kotlin

This repo contains two Kotlin functions to implement for a challenge: Second GreatLow, and Most Free Time.

## Run

*Requirements: docker, bash*

`./run-main.sh`

## Test

*Requirements: docker, bash*

`./run-tests.sh`